﻿using System;

namespace Melyakina.Lab6.Ex2
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = Convert.ToInt32(Console.ReadLine());
            int m = Convert.ToInt32(Console.ReadLine());

            int[,] X = new int[n, m];

            Random(X);

            Show(X);

            if (n != m)
            {
                Column2(X,n,m);

            }
            else
            {
                Column1(X);
            }


            Console.ReadLine();


        }
        public static void Show(int[,] array)
        {
            for (var i = 0; i < array.GetLength(0); i++)
            {
                for (var j = 0; j < array.GetLength(1); j++)
                {
                    Console.Write("{0,-3}" + "  ", array[i, j]);
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }
        public static void Random(int[,] array)
        {
            Random r = new Random();
            for (var i = 0; i < array.GetLength(0); i++)
            {
                for (var j = 0; j < array.GetLength(1); j++)
                {
                    array[i, j] = r.Next(-100, 100);
                }
            }
        }
        public static void Column1(int[,] array)
        {
            int H = 0;
            int n = array.GetLength(0);

            for (var i = 0; i < n; i++)
            {
                var x = array[i, n - 1];
                //Console.Write("{0, -3}" + "     ", x);

                if (x > 0)
                {
                    H += 1;
                }
                else
                {
                    Console.WriteLine("В столбце нет положительных чисел");
                }
            }
                if (H < 3)
                {
                    for (int k = 0; k < array.GetLength(0); k++)  
                    {
                        for (int t = 0; t < array.GetLength(1); t++)
                        {
                            if (array[k, t] > 0)
                            {
                                Console.Write($"{array[k, t]}" + "   ");

                            }
                        }
                    }


                    //Console.WriteLine($"{H})  {x} ");
                }
                else if (H == 3)
                {
                    // Console.WriteLine($"{H})  {x} ");
                    Console.WriteLine("H=3");
                }
                else
                {

                    ShowDiagonals(array);

                }
                
                Console.WriteLine();
                 
            
            
        }
        public static void ShowDiagonals(int[,] array)
        { int sum=0;
            int n = array.GetLength(0);
            for (var i = 0; i < n; i++)
            {
                var x = array[i, i];
                sum += x;
                Console.WriteLine(sum);

            }
            Console.WriteLine();
            
            
        }
         public static void Column2(int[,] array,int n,int m)
        {
            int H = 0;
            for (var i = 0; i < n; i++)
            {
                var x = array[i, m - 1];

                //Console.Write("{0, -3}" + "  "array[i,m-1], );
                if (x > 0)
                {
                    H += 1;
                }
                else
                {
                    Console.WriteLine("В столбце нет положительных чисел");
                }
            }
            if (H < 3)
            {
                for (int k = 0; k < n; k++)
                {
                    for (int t = 0; t < m; t++)
                    {
                        if (array[k, t] > 0)
                        {
                            Console.Write($"{array[k, t]}" + "   ");

                        }
                    }
                }


            }
            else if (H == 3)
            {
                
                Console.WriteLine("H=3");
            }
            else
            {

                Console.WriteLine("Невозможно вывести числа по диагонали");

            }

            Console.WriteLine();

          }
            //Console.WriteLine();
        }
    }
